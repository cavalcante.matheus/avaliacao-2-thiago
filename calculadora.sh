#!/bin/bash

echo "Digite o primeiro numero: "
read numero1

echo "Digite o segundo numero: "
read numero2

soma=$((numero1 + numero2))
subtracao=$((numero1 - numero2))
multiplicacao=$((numero1 * numero2))
divisao=$(echo "scale=2; $numero1 / $numero2" | bc)

echo "Digite a operação que você deseja: "
echo "1) Soma"
echo "2) Subtração"
echo "3) MUltiplicação"
echo "4) Divisão"
read operacao

test $operacao -eq 1 && echo $soma
test $operacao -eq 2 && echo $subtracao
test $operacao -eq 3 && echo $multiplicacao
test $operacao -eq 4 && echo $divisao
