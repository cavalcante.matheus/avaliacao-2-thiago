#!/bin/bash

quarta_1=$(date -d "next wednesday" +%d-%m-%Y)
quarta_2=$(date -d "next wednesday + 7 days" + %d-%m-%Y)
quarta_3=$(date -d "next wednesday + 14 days" + %d-%m-%Y)
quarta_4=$(date -d "next wednesday + 21 days" + %d-%m-%Y)

echo "Primeira quarta-feira: $quarta_1"
echo "Segunda quarta-feira: $quarta_2"
echo "Terceira quarta-feira: $quarta_3"
echo "Quarta quarta-feira: $quarta_4"

mkdir $quarta_1
mkdir $quarta_2
mkdir $quarta_3
mkdir $quarta_4
